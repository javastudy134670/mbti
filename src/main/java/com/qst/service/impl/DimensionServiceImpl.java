package com.qst.service.impl;

import java.util.List;

import com.qst.ExamException;
import com.qst.dao.DAOFactory;
import com.qst.dao.DimensionDAO;
import com.qst.dao.QuestionDAO;
import com.qst.entity.PersonalityDimension;
import com.qst.service.IDimensionService;

public class DimensionServiceImpl implements IDimensionService {
	private DimensionDAO dimensionDAO = DAOFactory.getDAO(DimensionDAO.class);
	private QuestionDAO questionDAO = DAOFactory.getDAO(QuestionDAO.class);

	@Override
	public List<PersonalityDimension> findDimensionByAssessment(int assessmentId) {
		return dimensionDAO.findByAssessment(assessmentId);
	}
	@Override
	public PersonalityDimension findDimensionById(int id) {
		return dimensionDAO.findById(id);
	}
	@Override
	public void saveDimension(PersonalityDimension kp) {
		PersonalityDimension temp = dimensionDAO.findByAssessmentAndTitle(kp.getAssessmentId(), kp.getTitle());
		if (temp == null) {
			dimensionDAO.insert(kp);

		} else {
			throw new ExamException("本知识点名称已存在");
		}
	}
	@Override
	public void updateDimension(PersonalityDimension kp) {
		PersonalityDimension temp = dimensionDAO.findByAssessmentAndTitle(kp.getAssessmentId(), kp.getTitle());
		if (temp == null || temp.getId() == kp.getId()) {
			dimensionDAO.update(kp);
		} else {
			throw new ExamException("本知识点名称已存在");
		}
	}
	@Override
	public void deleteDimension(int id) {
		int count = questionDAO.findCountByDimension(id);
		if (count > 0) {
			throw new ExamException("本知识点已录入考题，不能再删除");
		}
		dimensionDAO.delete(id);
	}

}
