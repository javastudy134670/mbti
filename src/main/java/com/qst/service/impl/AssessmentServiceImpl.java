package com.qst.service.impl;

import java.util.List;

import com.qst.ExamException;
import com.qst.dao.DAOFactory;
import com.qst.dao.QuestionDAO;
/*import com.qst.dao.QuestionDAO;*/
import com.qst.dao.AssessmentTypeDAO;
import com.qst.entity.AssessmentType;
import com.qst.service.IAssessmentService;

public class AssessmentServiceImpl implements IAssessmentService {
	private AssessmentTypeDAO assessmentDAO = DAOFactory.getDAO(AssessmentTypeDAO.class);
	private QuestionDAO questionDAO = DAOFactory.getDAO(QuestionDAO.class);

	@Override
	public List<AssessmentType> findAllAssessment() {
		return assessmentDAO.findAll();
	}
	@Override
	public AssessmentType findAssessmentById(int id) {
		return assessmentDAO.findById(id);
	}
	@Override
	public void updateAssessment(AssessmentType assessment) {
		AssessmentType temp = assessmentDAO.findByTitle(assessment.getTitle());
		if (temp != null && temp.getId() != assessment.getId()) {
			throw new ExamException("考核类型重复");
		}
		assessmentDAO.update(assessment);

	}
	@Override
	public void saveAssessment(AssessmentType assessment) {
		AssessmentType temp = assessmentDAO.findByTitle(assessment.getTitle());
		if (temp != null) {
			throw new ExamException("考核类型已存在");
		}
		assessmentDAO.insert(assessment);
	}
	@Override
	public void deleteAssessment(int id) {
		if (questionDAO.findCountByAssessment(id) > 0) {
			throw new ExamException("本考核类型已有试题，不能删除");
		}
		assessmentDAO.delete(id);
	}

}
