package com.qst.service.impl;

import java.util.List;

import com.qst.ExamException;
import com.qst.dao.DAOFactory;
import com.qst.dao.UserDAO;
import com.qst.entity.User;
import com.qst.service.IUserAdminService;

public class UserAdminServiceImpl implements IUserAdminService {
	private UserDAO userDAO = DAOFactory.getDAO(UserDAO.class);

	@Override
	public List<User> findUsers() {
		return userDAO.findAll();
	}

	@Override
	public User findUserById(int id) {

		return userDAO.findById(id);
	}
	@Override
	public void saveUser(User u) {
		User temp = userDAO.findByLogin(u.getLogin());
		if (temp != null) {
			throw new ExamException("登录名已经存在了");
		}
		userDAO.insert(u);
	}
	@Override
	public void updateUser(User user) {
		User temp = userDAO.findByLogin(user.getLogin());
		if (temp != null && temp.getId() != user.getId()) {
			throw new ExamException("登录名已经存在了");
		}
		userDAO.update(user);
	}
	@Override
	public void deleteUserById(int id) {
		User user = userDAO.findById(id);
		if (user == null) {
			return;
		}
		if (user.getLastLogin() != null) {
			throw new ExamException("该用户不能被删除");
		}
		userDAO.delete(id);

	}
	@Override
	public void resetPassword(int id) {
		userDAO.updatePassword(id, User.PASSWORD);

	}

}
