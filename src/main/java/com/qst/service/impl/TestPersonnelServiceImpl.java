package com.qst.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.qst.ExamException;
import com.qst.dao.DAOFactory;
import com.qst.dao.TestPersonnelDAO;
import com.qst.dao.UserDAO;
import com.qst.entity.TestPersonnel;
import com.qst.entity.User;
import com.qst.service.ITestPersonnelService;

public class TestPersonnelServiceImpl implements ITestPersonnelService {
	private TestPersonnelDAO personDAO = DAOFactory.getDAO(TestPersonnelDAO.class);
	private UserDAO userDAO = DAOFactory.getDAO(UserDAO.class);

	@Override
	public List<TestPersonnel> findByTeamId(Integer id) {
		try {
			return personDAO.findByTeam(id);
		} catch (SQLException ex) {
			throw new ExamException("数据库查询出错");
		}
	};

	@Override
	public TestPersonnel findById(int id) {
		try {
			TestPersonnel stu = personDAO.findById(id);
			stu.setUser(userDAO.findById(id));
			return stu;
		} catch (SQLException ex) {
			throw new ExamException("数据库查询出错", ex);
		}
	}
	 @Override
	    public List<TestPersonnel> query(int teamId,String name,String stdNo){
			try {
				return personDAO.query(teamId,name,stdNo);
			} catch (SQLException ex) {
				throw new ExamException("不存在该参测人员");
			}
	    }
	@Override
	public void updateTestPersonnel(TestPersonnel s) {
		try {
			User user = s.getUser();
			User u = userDAO.findById(s.getId());
			u.setName(user.getName());
			if (user.getStatus() != 0) {
				u.setStatus(user.getStatus());
			}
			userDAO.update(u);
			personDAO.update(s);
		} catch (SQLException ex) {
			throw new ExamException("更新参测人员出错", ex);
		}
	}
	@Override
	public void importTestPersonnel(List<TestPersonnel> testPersonnelList) {
		try {
			for (int i = 0; i < testPersonnelList.size(); i++) {
				TestPersonnel s = testPersonnelList.get(i);
				TestPersonnel temp = personDAO.findByphone(s.getPhone());
				if (temp == null)// 添加学生
				{
					addTestPersonnel(s);
				} else // 更新已存在学生
				{
					if (temp.getTeamId() != s.getTeamId()) {
						throw new ExamException("导入第" + (i + 1) + "个参测人员出错,该手机号"
								+ s.getPhone() + "已在其他参测人员注册");
					}
					s.setId(temp.getId());
					updateTestPersonnel(s);

				}
			}
		} catch (SQLException ex) {
			throw new ExamException("导入参测人员出错", ex);
		}
	}
	@Override
	public void addTestPersonnel(TestPersonnel s) {
		try {
			User u = s.getUser();
			if (u.getType() == 0) {
				u.setType(4);
			}

			u.setStatus(1);
			if (u.getPasswd() == null || u.getPasswd().equals("")) {
				u.setPasswd(User.PASSWORD);
			}

			u.setLogin(s.getPhone());// 学号作为默认登录名
			userDAO.insert(u);
			s.setId(u.getId());
			personDAO.insert(s);
		} catch (SQLException ex) {
			throw new ExamException("添加参测人员出错", ex);
		}
	}

	
	


	@Override
	public TestPersonnel deleteTestPersonnel(int id) {
		try {

			User user = userDAO.findById(id);
			if (user.getLastLogin() != null) {
				throw new ExamException("该账户已启用，不能删除");
			}
			TestPersonnel testPersonnel = personDAO.findById(id);
			personDAO.delete(id);
			userDAO.delete(id);

			testPersonnel.setUser(user);
			return testPersonnel;
		} catch (SQLException e) {
			throw new ExamException("删除用户出错", e);
		}
	}
}
