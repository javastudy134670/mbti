package com.qst.service.impl;

import java.util.List;

import com.qst.ExamException;
import com.qst.action.question.QuestionQueryParam;
import com.qst.dao.ChoiceDAO;
/*import com.qst.dao.ChoiceDAO;*/
import com.qst.dao.DAOFactory;
import com.qst.dao.DimensionDAO;
import com.qst.dao.QuestionDAO;
import com.qst.entity.Choice;
import com.qst.entity.PersonalityDimension;
import com.qst.entity.Question;
import com.qst.service.IQuestionService;

public class QuestionServiceImpl implements IQuestionService {
	private QuestionDAO questionDAO = DAOFactory.getDAO(QuestionDAO.class);
	private ChoiceDAO choiceDAO = DAOFactory.getDAO(ChoiceDAO.class);
	private DimensionDAO dimensionDAO = DAOFactory.getDAO(DimensionDAO.class);

	@Override
	public List<Question> find(QuestionQueryParam param) {

		List<Question> questions = questionDAO.find(param);
		// for (Question q : questions) {
		// q.setChoices(choiceDAO.findByQuestion(q.getId()));
		// }
		return questions;

	}
	@Override
	public Question findById(int id) {

		return questionDAO.findById(id);
	}

	@Override
	public List<Choice> findChoices(Integer id) {

		return choiceDAO.findByQuestion(id);
	}
	@Override
	public List<PersonalityDimension> findDimensionByQuestion(Integer id) {

		return dimensionDAO.findDimensionByQuestion(id);
	}
	@Override
	public void save(Question q, List<Choice> choices) {
		isChoiceValid(q.getType(), choices);
		q.setStatus(2);
		// 这个insert方法里有获取考题id的逻辑
		questionDAO.insert(q);
		for (Choice ch : choices) {
			// 上面的insert方法里有获取考题id的逻辑
			ch.setQuestionId(q.getId());
			choiceDAO.insert(ch);
		}
	}
	private void isChoiceValid(int type, List<Choice> choices) {
		int count = 0;
		for (Choice ch : choices) {
			if (ch.isChecked()) {
				count++;
			}
		}
		if (count == 0) {
			throw new ExamException("请选择本题的正确选项");
		}
		if (type == 1 && count != 1) {
			throw new ExamException("单选题只能有一个正确选项");
		}
		if (type == 2 && count < 2) {
			throw new ExamException("多选题至少要有两个正确选项");
		}
	}
	@Override
	public void update(Question q, List<Choice> choices) {
		isChoiceValid(q.getType(), choices);
		questionDAO.update(q);
		for (Choice ch : choices) {
			ch.setQuestionId(q.getId());
			choiceDAO.update(ch);
		}
	}
	@Override
	public void delete(int questionId) {
		if (questionDAO.isUsed(questionId)) {
			throw new ExamException("使用中的试题，不能被删除");
		}
		questionDAO.detachDimension(questionId);
		choiceDAO.deleteByQuestion(questionId);
		questionDAO.delete(questionId);
	}

	@Override
	public void attachDimension(int questionId, int[] dimension) {
		questionDAO.detachDimension(questionId);
		for (int p : dimension) {
			questionDAO.attachDimension(questionId, p);
		}
	}
	@Override
	public List<Integer> findDimensionIdByQuestion(int id) {
		return questionDAO.findDimension(id);
	}
}
