package com.qst.service;

import java.util.List;

import com.qst.entity.PersonalityDimension;

public interface IDimensionService {

	List<PersonalityDimension> findDimensionByAssessment(int assessmentId);
	PersonalityDimension findDimensionById(int id);
	void saveDimension(PersonalityDimension kp);
	void updateDimension(PersonalityDimension kp);
	void deleteDimension(int id);
}
