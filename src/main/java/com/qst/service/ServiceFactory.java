package com.qst.service;

import java.util.HashMap;
import java.util.Map;

import com.qst.service.impl.AssessmentServiceImpl;
import com.qst.service.impl.DimensionServiceImpl;
import com.qst.service.impl.ExamServiceImpl;
import com.qst.service.impl.QuestionServiceImpl;
import com.qst.service.impl.ScheduleServiceImpl;
import com.qst.service.impl.TeamServiceImpl;
import com.qst.service.impl.TestPersonnelServiceImpl;
import com.qst.service.impl.UserAdminServiceImpl;
import com.qst.service.impl.UserServiceImpl;

public class ServiceFactory {
	private static Map<Class, Object> services = new HashMap<Class, Object>();
	static {
		
		
	/*	
		
		
		
		*/
		services.put(IScheduleService.class, new ScheduleServiceImpl());
		services.put(IQuestionService.class, new QuestionServiceImpl());
		services.put(IDimensionService.class, new DimensionServiceImpl());
		services.put(IAssessmentService.class, new AssessmentServiceImpl());
		services.put(IUserAdminService.class, new UserAdminServiceImpl());
		services.put(IUserService.class, new UserServiceImpl());
		services.put(ITeamService.class, new TeamServiceImpl());
		services.put(ITestPersonnelService.class, new TestPersonnelServiceImpl());
		services.put(IExamService.class, new ExamServiceImpl());
	/*	
		
		
		*/
	}

	public static <T> T getService(Class<T> clazz) {
		return (T) services.get(clazz);
	}
}
