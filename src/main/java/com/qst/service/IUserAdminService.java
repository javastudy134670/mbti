package com.qst.service;

import java.util.List;

import com.qst.entity.User;

public interface IUserAdminService {

	List<User> findUsers();
	User findUserById(int id);
	void saveUser(User u);
	void updateUser(User user);
	void deleteUserById(int id);
	void resetPassword(int id);

}
