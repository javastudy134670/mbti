package com.qst.service;

import java.util.List;

import com.qst.entity.Schedule;
import com.qst.entity.TestPersonnel;
import com.qst.entity.User;

public interface IScheduleService {
	List<Schedule> findByCreator(User user);
	Schedule findById(Integer id);
	void saveSchedule(Schedule h);
	void updateSchedule(Schedule h);
	Schedule deleteSchedule(Integer id);

}
