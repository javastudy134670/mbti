package com.qst.service;

import java.util.List;

import com.qst.entity.AssessmentType;

public interface IAssessmentService {
	List<AssessmentType> findAllAssessment();
	AssessmentType findAssessmentById(int id);
	void updateAssessment(AssessmentType assessmentType);
	void saveAssessment(AssessmentType assessmentType);
	void deleteAssessment(int id);
}
