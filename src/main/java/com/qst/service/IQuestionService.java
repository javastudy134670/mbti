package com.qst.service;

import java.util.List;

import com.qst.action.question.QuestionQueryParam;
import com.qst.entity.Choice;
import com.qst.entity.PersonalityDimension;
import com.qst.entity.Question;

public interface IQuestionService {

	List<Question> find(QuestionQueryParam param);
	Question findById(int id);
	List<Choice> findChoices(Integer id);
	List<PersonalityDimension> findDimensionByQuestion(Integer id);
	void save(Question q, List<Choice> choices);
	List<Integer> findDimensionIdByQuestion(int id);
	void attachDimension(int questionId, int[] dimension);
	void update(Question q, List<Choice> choices);
	void delete(int questionId);

/*	

	

	

	

	
	

	

	*/

}
