package com.qst.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.qst.Db;
import com.qst.ExamException;
import com.qst.entity.AssessmentType;

public class AssessmentTypeDAO {

	public List<AssessmentType> findAll() {
		String sql = "select id,title,cost,status from assessments";
		List<AssessmentType> assessments = new ArrayList<>();
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql);
				ResultSet rs = stmt.executeQuery()) {
			while (rs.next()) {
				assessments.add(createAssessmentType(rs));
			}
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
		return assessments;
	}
	public int insert(AssessmentType sj) {

		String sql = "insert into  assessments (title,cost,status) values(?,?,?)";

		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql,
						Statement.RETURN_GENERATED_KEYS)) {
			stmt.setObject(1, sj.getTitle());
			stmt.setObject(2, sj.getCost());
			stmt.setObject(3, sj.getStatus());
			stmt.executeUpdate();
			sj.setId(Db.getGeneratedInt(stmt));
			return sj.getId();
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}

	public void delete(int id) {
		String sql = "delete from assessments where id=?";
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, id);
			stmt.executeUpdate();
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}
	public AssessmentType findByTitle(String title) {
		String sql = "select id,title,cost,status from assessments where title=?";
		AssessmentType assessment = null;
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, title);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				assessment = createAssessmentType(rs);
			}
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
		return assessment;
	}
	public void update(AssessmentType sj) {
		String sql = "update  assessments set title=?,cost=?,status=? where id=?";

		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, sj.getTitle());
			stmt.setObject(2, sj.getCost());
			stmt.setObject(3, sj.getStatus());
			stmt.setObject(4, sj.getId());
			stmt.executeUpdate();
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}
	private AssessmentType createAssessmentType(ResultSet rs) throws SQLException {
		AssessmentType sj = new AssessmentType();
		sj.setId(rs.getInt("id"));
		sj.setTitle(rs.getString("title"));
		sj.setCost(rs.getDouble("cost"));
		sj.setStatus(rs.getInt("status"));
		return sj;
	}

	public AssessmentType findById(int id) {
		String sql = "select id,title,cost,status from assessments where id=?";
		AssessmentType assessment = null;
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				assessment = createAssessmentType(rs);
			}
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
		return assessment;
	}
}
