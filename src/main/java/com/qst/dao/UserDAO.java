package com.qst.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.qst.Db;
import com.qst.ExamException;
import com.qst.entity.User;

public class UserDAO {

	public List<User> findAll() {
		String sql = "select id,login,name,passwd,type,status from users where type<4";
		List<User> users = new ArrayList<>();
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql);
				ResultSet rs = stmt.executeQuery()) {
			while (rs.next()) {
				users.add(createUser(rs));
			}
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
		return users;
	}

	public User findByLogin(String login) {
		String sql = "select id,login,name,passwd,type,status from users where login=?";
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, login);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return createUser(rs);
			}
			return null;
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}

	public User findById(int id) {
		String sql = "select id,login,name,passwd,type,status from users where id=?";
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return createUser(rs);
			}
			return null;
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}
	public int insert(User u) {
		String sql = "insert into users(login,passwd,name,type,status) values(?,?,?,?,?)";
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql,
						Statement.RETURN_GENERATED_KEYS)) {
			stmt.setObject(1, u.getLogin());
			stmt.setObject(2, u.getPasswd());
			stmt.setObject(3, u.getName());
			stmt.setObject(4, u.getType());
			stmt.setObject(5, u.getStatus());
			stmt.executeUpdate();
			u.setId(Db.getGeneratedInt(stmt));
			return u.getId();
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}
	public void update(User u) {
		String sql = "update users set login=?,name=?,type=?,status=? where id=?";
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, u.getLogin());
			stmt.setObject(2, u.getName());
			stmt.setObject(3, u.getType());
			stmt.setObject(4, u.getStatus());
			stmt.setObject(5, u.getId());
			stmt.executeUpdate();
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}
	
	public void updatePassword(int userId, String password) {
		String sql = "update users set passwd=? where id=?";
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, password);
			stmt.setObject(2, userId);
			stmt.executeUpdate();
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}
	public void delete(int userId) {
		String sql = "delete from users where id=?";
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, userId);
			stmt.executeUpdate();
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}
	public void updateLastLogin(Integer userId, Timestamp lastLogin) {
		String sql = "update users set last_login=? where id=?";
		try (Connection conn = Db.getConnection();
				PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setObject(1, lastLogin);
			stmt.setObject(2, userId);
			stmt.executeUpdate();
		} catch (SQLException ex) {
			throw new ExamException(sql, ex);
		}
	}

	public User createUser(ResultSet rs) throws SQLException {
		return new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
				rs.getInt(5), rs.getInt(6));
	}
}
