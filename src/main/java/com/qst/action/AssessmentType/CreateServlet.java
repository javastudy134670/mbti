package com.qst.action.AssessmentType;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.WebUtil;

@WebServlet("/assessment/create.action")
public class CreateServlet extends BaseServlet {
	// private IassessmentService assessmentService =
	// ServiceFactory.getService(IassessmentService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		WebUtil.forward(req, resp, "/assessment/create.jsp");
	}
}
