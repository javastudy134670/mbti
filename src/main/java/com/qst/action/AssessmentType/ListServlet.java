package com.qst.action.AssessmentType;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.WebUtil;
import com.qst.entity.AssessmentType;
import com.qst.service.IAssessmentService;
import com.qst.service.ServiceFactory;

@WebServlet("/assessment/list.action")
public class ListServlet extends BaseServlet {
	private IAssessmentService assessmentService = ServiceFactory
			.getService(IAssessmentService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<AssessmentType> assessmentList = assessmentService.findAllAssessment();
		req.setAttribute("assessmentList", assessmentList);
		WebUtil.forward(req, resp, "/assessment/list.jsp");
	}
}
