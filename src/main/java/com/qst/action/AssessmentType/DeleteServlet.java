package com.qst.action.AssessmentType;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.ExamException;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.service.IAssessmentService;
import com.qst.service.ServiceFactory;

@WebServlet("/assessment/delete.action")
public class DeleteServlet extends BaseServlet {
	private IAssessmentService assessmentService = ServiceFactory.getService(IAssessmentService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		int id = RequestUtil.getInt(req, "id");

		try {
			assessmentService.deleteAssessment(id);
			addMessage(req, "科目已删除");
		} catch (ExamException ex) {
			addError(req, ex.getMessage());
		}
		WebUtil.redirect(req, resp, "/assessment/list.action");
	}
}
