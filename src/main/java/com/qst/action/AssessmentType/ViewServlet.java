package com.qst.action.AssessmentType;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.entity.AssessmentType;
import com.qst.service.IAssessmentService;
import com.qst.service.ServiceFactory;

@WebServlet("/assessment/view.action")
public class ViewServlet extends BaseServlet {
	private IAssessmentService assessmentService = ServiceFactory
			.getService(IAssessmentService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		int id = RequestUtil.getInt(req, "id");
		// String str = req.getParameter("id");
		// int id = Integer.parseInt(str);
		AssessmentType assessment = assessmentService.findAssessmentById(id);
		req.setAttribute("assessment", assessment);
		WebUtil.forward(req, resp, "/assessment/view.jsp");
	}
}
