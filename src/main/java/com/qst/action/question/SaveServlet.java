package com.qst.action.question;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.Constant;
import com.qst.ExamException;
import com.qst.WebUtil;
import com.qst.entity.Choice;
import com.qst.entity.Question;
import com.qst.entity.User;
import com.qst.service.IQuestionService;
import com.qst.service.IAssessmentService;
import com.qst.service.ServiceFactory;

@WebServlet("/question/save.action")
public class SaveServlet extends BaseServlet {
	private IAssessmentService assessmentService = ServiceFactory.getService(IAssessmentService.class);
	private IQuestionService questionService = ServiceFactory.getService(IQuestionService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Question question = QuestionHelper.createQuestion(req);
		List<Choice> choices = QuestionHelper.createChoice(req);
		try {
			// todo 完成登陆后更改此处为当前用户
			User currentUser = (User) req.getSession().getAttribute(Constant.CURRENT_USER);
			question.setUserId(currentUser.getId());
			questionService.save(question, choices);
			addMessage(req, "试题保存到数据库中");
			WebUtil.redirect(req, resp, "/question/dimension.action?id=" + question.getId());
		} catch (ExamException ex) {
			System.out.println(ex);
			addError(req, ex.getMessage());
			req.setAttribute("question", question);
			req.setAttribute("choices", choices);
			req.setAttribute("assessment", assessmentService.findAssessmentById(question.getAssessmentId()));
			WebUtil.forward(req, resp, "/question/create.jsp");
		}
	}
}
