package com.qst.action.question;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.entity.PersonalityDimension;
import com.qst.entity.Question;
import com.qst.service.IDimensionService;
import com.qst.service.IQuestionService;
import com.qst.service.ServiceFactory;

@WebServlet("/question/dimension.action")
public class DimensionServlet extends BaseServlet {
	private IQuestionService questionService = ServiceFactory.getService(IQuestionService.class);
	private IDimensionService dimensionService = ServiceFactory.getService(IDimensionService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String method = RequestUtil.getString(req, "method");

		if ("save".equals(method)) {
			save(req, resp);
		} else {
			edit(req, resp);
		}
	}

	protected void edit(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException {
		int id = RequestUtil.getInt(req, "id");
		Question question = questionService.findById(id);
		req.setAttribute("question", question);
		List<PersonalityDimension> dimensionList = dimensionService.findDimensionByAssessment(question.getAssessmentId());
		req.setAttribute("dimensionList", dimensionList);
		List<Integer> attachedDimension = questionService.findDimensionIdByQuestion(id);
		for (PersonalityDimension p : dimensionList) {
			p.getExtras().put("checked", attachedDimension.contains(p.getId()) ? "checked" : "");
		}
		WebUtil.forward(req, resp, "/question/dimension.jsp");
	}

	protected void save(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException {
        int qid = RequestUtil.getInt(req, "id");
        if(qid==0){
            qid=RequestUtil.getInt(req, "qid");
        }
        int[] pids = RequestUtil.getIntArray(req, "pid");
        if(pids==null){
            	Question question = questionService.findById(qid);
            List<PersonalityDimension> dimensionList = dimensionService.findDimensionByAssessment(question.getAssessmentId());
		req.setAttribute("dimensionList", dimensionList);
		List<Integer> attachedDimension = questionService.findDimensionIdByQuestion(qid);
		for (PersonalityDimension p : dimensionList) {
			p.getExtras().put("checked", attachedDimension.contains(p.getId()) ? "checked" : "");
        }
        req.setAttribute("error", "你还未关联任何维度！");
        req.setAttribute("qid", qid);
		WebUtil.forward(req, resp, "/question/dimension.jsp");
        }
            questionService.attachDimension(qid, pids);
		    WebUtil.redirect(req, resp, "/question/view.action?id=" + qid);
        	
	}

}
