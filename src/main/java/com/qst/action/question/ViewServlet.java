package com.qst.action.question;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.entity.Choice;
import com.qst.entity.PersonalityDimension;
import com.qst.entity.Question;
import com.qst.entity.AssessmentType;
import com.qst.service.IQuestionService;
import com.qst.service.IAssessmentService;
import com.qst.service.ServiceFactory;

@WebServlet("/question/view.action")
public class ViewServlet extends BaseServlet {
	private IAssessmentService assessmentService = ServiceFactory.getService(IAssessmentService.class);
	private IQuestionService questionService = ServiceFactory.getService(IQuestionService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Question question = questionService.findById(RequestUtil.getInt(req, "id"));
		List<Choice> choices = questionService.findChoices(question.getId());
		AssessmentType assessment = assessmentService.findAssessmentById(question.getAssessmentId());
		List<PersonalityDimension> dimension = questionService.findDimensionByQuestion(question.getId());
		req.setAttribute("assessment", assessment);
		req.setAttribute("question", question);
		req.setAttribute("choices", choices);
		req.setAttribute("dimension", dimension);

		WebUtil.forward(req, resp, "/question/view.jsp");
	}
}
