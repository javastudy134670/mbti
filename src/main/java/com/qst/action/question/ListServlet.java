package com.qst.action.question;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.entity.Question;
import com.qst.entity.AssessmentType;
import com.qst.service.IQuestionService;
import com.qst.service.IAssessmentService;
import com.qst.service.ServiceFactory;

@WebServlet("/question/list.action")
public class ListServlet extends BaseServlet {
	private IAssessmentService assessmentService = ServiceFactory
			.getService(IAssessmentService.class);
	private IQuestionService questionService = ServiceFactory
			.getService(IQuestionService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String submitButton = RequestUtil.getString(req, "create");
		
		if (submitButton.length() > 0)// 添加操作
		{
			doCreate(req, resp);
		} else {
			doList(req, resp);
		}
	}

	protected void doList(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<AssessmentType> assessmentList = assessmentService.findAllAssessment();
		req.setAttribute("assessmentList", assessmentList);

		QuestionQueryParam param = QuestionQueryParam.create(req);
		req.setAttribute("query", param);
		// if (param.getAssessmentId() > 0) {
			List<Question> questionList = questionService.find(param);
			req.setAttribute("questionList", questionList);
		// }
		WebUtil.forward(req, resp, "/question/list.jsp");
	}

	protected void doCreate(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		QuestionQueryParam param = QuestionQueryParam.create(req);
		req.setAttribute("assessment",
				assessmentService.findAssessmentById(param.getAssessmentId()));
		Question question = new Question();
		question.setType(param.getType());
		question.setDifficulty(4);
		req.setAttribute("question", question);
		WebUtil.forward(req, resp, "/question/create.jsp");
	}
}
