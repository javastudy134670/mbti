package com.qst.action.TestPersonnel;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.entity.Team;
import com.qst.entity.TestPersonnel;
import com.qst.service.ITeamService;
import com.qst.service.ITestPersonnelService;
import com.qst.service.ServiceFactory;

@WebServlet("/testPersonnel/list.action")
public class ListServlet extends BaseServlet {
    private ITeamService classTeamService = ServiceFactory
            .getService(ITeamService.class);
    private ITestPersonnelService testPersonnelService = ServiceFactory
            .getService(ITestPersonnelService.class);

    @Override
    protected void doAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 获取班级id
        int tid = RequestUtil.getInt(req, "tid");

        List<TestPersonnel> testPersonnelList = testPersonnelService.findByTeamId(tid);
        req.setAttribute("testPersonnel", testPersonnelList);
        List<Team> teamList = classTeamService.findAll();
        req.setAttribute("teamList", teamList);

        WebUtil.forward(req, resp, "/testPersonnel/list.jsp");
    }

}
