package com.qst.action.TestPersonnel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.qst.BaseServlet;
import com.qst.Constant;
import com.qst.ExamException;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.dao.DAOFactory;
import com.qst.dao.UserDAO;
import com.qst.entity.TestPersonnel;
import com.qst.entity.User;
import com.qst.service.ITeamService;
import com.qst.service.ITestPersonnelService;
import com.qst.service.IUserAdminService;
import com.qst.service.ServiceFactory;

@WebServlet("/testPersonnel/add.action")
@MultipartConfig
public class AddServlet extends BaseServlet {
	private ITestPersonnelService testPersonnelService = ServiceFactory
			.getService(ITestPersonnelService.class);
private UserDAO dao=DAOFactory.getDAO(UserDAO.class);
	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		User user = new User();
		req.getParameter("");
		user.setId(RequestUtil.getInt(req, "id"));
		user.setLogin(RequestUtil.getString(req, "login"));
		user.setPasswd(User.PASSWORD);
		user.setName(RequestUtil.getString(req, "name"));
		user.setType(RequestUtil.getInt(req, "type"));
		user.setStatus(RequestUtil.getInt(req, "status"));
		TestPersonnel s = new TestPersonnel();
		s.setUser(user);
		s.setGender(RequestUtil.getString(req, "gender"));
		s.setPhone(RequestUtil.getString(req, "login"));
		s.setTeamId(RequestUtil.getInt(req, "teamId"));
		s.setBirthDate(RequestUtil.getDate(req, "birthDate"));
		try {
            if(dao.findByLogin(s.getPhone())!=null){
                addMessage(req, "该手机号已经存在！");
                int teamId=RequestUtil.getInt(req, "teamId");
                req.setAttribute("teamId", teamId);
				WebUtil.forward(req,resp,"/testPersonnel/create.jsp");
			}else{
			testPersonnelService.addTestPersonnel(s);
			addMessage(req, "参测人员添加成功");
			WebUtil.redirect(req, resp, "/testPersonnel/select.action?teamId="+s.getTeamId());}
		} catch (ExamException ex) {
			req.setAttribute("user", user);
			addError(req, ex.getMessage());
			WebUtil.forward(req, resp, "/testPersonnel/create.jsp");
		}
	}
}
