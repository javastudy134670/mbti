package com.qst.action.TestPersonnel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.qst.BaseServlet;
import com.qst.ExamException;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.entity.TestPersonnel;
import com.qst.entity.User;
import com.qst.service.ITeamService;
import com.qst.service.ITestPersonnelService;
import com.qst.service.ServiceFactory;

@WebServlet("/testPersonnel/import.action")
@MultipartConfig
public class ImportServlet extends BaseServlet {
	private ITeamService classTeamService = ServiceFactory
			.getService(ITeamService.class);
	private ITestPersonnelService testPersonnelService = ServiceFactory
			.getService(ITestPersonnelService.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		WebUtil.forward(req, resp, "/testPersonnel/import.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		int tid = RequestUtil.getInt(req, "tid");
		Part part = req.getPart("file");
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				part.getInputStream()));
		String line;
		List<TestPersonnel> testPersonnelList = new ArrayList<>();
		while ((line = reader.readLine()) != null) {
			String[] data = line.split(",");
			if (data.length < 4) {
				addError(req, "导入文件格式错误:" + line);
				WebUtil.forward(req, resp, "/testPersonnel/import.jsp");
				return;
			}
			TestPersonnel testPersonnel = new TestPersonnel();
			User user = new User();
			testPersonnel.setTeamId(tid);
			testPersonnel.setUser(user);
			testPersonnel.setPhone(data[0]);
			user.setName(data[1]);
			testPersonnel.setGender(data[2]);
			testPersonnel.setBirthDate(Date.valueOf(data[3]));
			testPersonnelList.add(testPersonnel);
		}
		try {
			testPersonnelService.importTestPersonnel(testPersonnelList);
			addMessage(req, "成功导入" + testPersonnelList.size() + "个参测人员");
			WebUtil.redirect(req, resp, "/testPersonnel/list.action?tid=" + tid);
		} catch (ExamException ex) {
			addError(req, ex.getMessage());
			WebUtil.redirect(req, resp, "/team/list.action");
		}
	}

}
