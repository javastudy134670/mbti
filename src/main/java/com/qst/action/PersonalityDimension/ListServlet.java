package com.qst.action.PersonalityDimension;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.entity.PersonalityDimension;
import com.qst.entity.AssessmentType;
import com.qst.service.IDimensionService;
import com.qst.service.IAssessmentService;
import com.qst.service.ServiceFactory;

@WebServlet("/dimension/list.action")
public class ListServlet extends BaseServlet {
	private IAssessmentService assessmentService = ServiceFactory
			.getService(IAssessmentService.class);
	private IDimensionService dimensionService = ServiceFactory
			.getService(IDimensionService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String submitButton = RequestUtil.getString(req, "create");
		if (submitButton.length() > 0)// 添加操作
		{
			doCreate(req, resp);
		} else {
			doList(req, resp);
		}
	}

	private void doCreate(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		int assessmentId = RequestUtil.getInt(req, "id");
		AssessmentType assessment = assessmentService.findAssessmentById(assessmentId);
		req.setAttribute("assessment", assessment);
		WebUtil.forward(req, resp, "/dimension/create.jsp");
	}

	private void doList(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setAttribute("assessmentList", assessmentService.findAllAssessment());
		// 如果是点击左边导航栏进来，则没有ID，所以就不显示知识点列表
		// 如果是点击查看知识点按钮进来的，就有科目ID，所以就显示该科目下的知识点列表
		int id = RequestUtil.getInt(req, "id");
		// if (id > 0) {
			List<PersonalityDimension> dimensionList = dimensionService
					.findDimensionByAssessment(id);
			req.setAttribute("dimensionList", dimensionList);
		// }
		WebUtil.forward(req, resp, "/dimension/list.jsp");
	}
}
