package com.qst.action.PersonalityDimension;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qst.BaseServlet;
import com.qst.ExamException;
import com.qst.RequestUtil;
import com.qst.WebUtil;
import com.qst.service.IDimensionService;
import com.qst.service.ServiceFactory;

@WebServlet("/dimension/delete.action")
public class DeleteServlet extends BaseServlet {
	private IDimensionService dimensionService = ServiceFactory.getService(IDimensionService.class);

	@Override
	protected void doAction(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		int id = RequestUtil.getInt(req, "id");
		try {
			dimensionService.deleteDimension(id);
			addMessage(req, "知识点已删除");
		} catch (ExamException ex) {
			ex.printStackTrace();
			addError(req, ex.getMessage());
		}
		WebUtil.redirect(req, resp, "/dimension/list.action?id=" + RequestUtil.getInt(req, "assessmentId"));
	}
}
